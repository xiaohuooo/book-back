import { message as AntdMessage } from "antd";
import axios from "axios";

export const CreateAxiosInstance = (config) => {
  const instance = axios.create({
    timeout: 5000,
    withCredentials: true,
    ...config,
  });

  instance.interceptors.request.use(
    function (config) {
      // 合并请求头
      const user = JSON.parse(localStorage.getItem("user") || "{}");
      config.headers = {
        userToken: user?._id,
      };
      return config;
    },
    function (error) {
      // 处理错误请求
      return Promise.reject(error);
    }
  );

  instance.interceptors.response.use(
    function (response) {
      // todo
      const { status, data, message } = response;
      if (status === 200) {
        return data;
      } else if (status === 401) {
        return (window.location.href = "/login");
      } else {
        AntdMessage.error(message);
        return Promise.reject(response.data);
      }
    },
    function (error) {
      if (error.response) {
        if (error.response.status === 401) {
          return (window.location.href = "/login");
        }
      }
      AntdMessage.error(error?.response?.data?.message || "服务端异常");
      return Promise.reject(error);
    }
  );

  return instance;
};

const request = CreateAxiosInstance({});
export default request;
