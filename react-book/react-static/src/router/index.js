import { useRoutes } from "react-router-dom";
import Home from "../views/home";
import Login from "../views/login";
import Book from "../components/book";
import Add from "../components/book/add";
import Category from "../views/category";
import Sell from "../views/sell";
import Add1 from "../views/sell/add";

const GetRoutes = () => {
  const routes = useRoutes([
    {
      path: "/",
      element: <Home />,
      children: [
        { path: "/book", element: <Book /> },
        { path: "/category", element: <Category /> },
        { path: "/sell", element: <Sell /> },
        { path: "/sell/add", element: <Add1 /> },
        { path: "/book/add", element: <Add /> },
      ],
    },
    {
      path: "/login",
      element: <Login />,
    },
    {
      path: "/*",
      element: <Home />,
    },
  ]);
  return routes;
};
export default GetRoutes;
