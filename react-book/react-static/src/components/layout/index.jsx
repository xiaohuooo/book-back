import { setLogout } from "../../api";
import { useCurrentUser } from "../../utils/hook";
import { DownOutlined } from "@ant-design/icons";
import {
  ProfileOutlined,
  SnippetsOutlined,
  SolutionOutlined,
} from "@ant-design/icons";
import { Layout as AntdLayout, Dropdown, Menu, Space, message } from "antd";

import { useNavigate, useLocation } from "react-router-dom";

import React, { useMemo } from "react";

import styles from "./index.module.scss";

const { Header, Sider, Content } = AntdLayout;

const ITEMS = [
  {
    label: "图书管理",
    key: "book",
    role: "user",
    icon: <SnippetsOutlined />,
    children: [
      {
        label: "图书列表",
        key: "/book",
        role: "user",
      },
      {
        label: "图书添加",
        key: "/book/add",
        role: "admin",
      },
    ],
  },
  {
    label: "售卖管理",
    key: "sell",
    role: "user",
    icon: <SolutionOutlined />,
    children: [
      {
        label: "售卖列表",
        key: "/sell",
        role: "user",
      },
      {
        label: "书籍售卖",
        key: "/sell/add",
        role: "admin",
      },
    ],
  },
  {
    label: "分类管理",
    key: "/category",
    icon: <ProfileOutlined />,
    role: "admin",
  },
];

const Layout = ({ children, title = "图书列表", operation }) => {
  const router = useNavigate();
  const location = useLocation();
  const user = useCurrentUser();

  const activeMenu = location.pathname;
  const defaultOpenKeys = [activeMenu.split("/")[1]];

  const handleChangeMenu = (e) => {
    router(e.key);
  };

  const USER_ITEMS = [
    {
      key: "2",
      label: (
        <span
          onClick={async () => {
            await setLogout();
            localStorage.removeItem("user");
            message.success("退出成功");
            router("/login");
          }}
        >
          退出
        </span>
      ),
    },
  ];

  const items = useMemo(() => {
    if (user?.role === "user") {
      return ITEMS.filter((item) => {
        if (item.children) {
          item.children = item.children.filter((k) => k.role === "user");
        }
        return item.role === "user";
      });
    } else {
      return ITEMS;
    }
  }, [user]);

  return (
    <>
      <main className={styles.main}>
        <AntdLayout className={styles.container}>
          <Header className={styles.header}>
            图书管理系统
            <span className={styles.user}>
              <Dropdown menu={{ items: USER_ITEMS }} placement="bottom">
                <span onClick={(e) => e.preventDefault()}>
                  <Space>
                    {user?.nickName}
                    <DownOutlined />
                  </Space>
                </span>
              </Dropdown>
            </span>
          </Header>
          <AntdLayout>
            <Sider className={styles.sider}>
              <Menu
                className={styles.menu}
                onClick={handleChangeMenu}
                selectedKeys={[activeMenu]}
                items={items}
                mode="inline"
                theme="light"
                defaultOpenKeys={defaultOpenKeys}
              />
            </Sider>
            <Content className={styles.content}>{children}</Content>
          </AntdLayout>
        </AntdLayout>
      </main>
    </>
  );
};

export default Layout;
