import request from "../utils/axios";
import qs from "qs";

export const setLogout = async () => {
  await request.get(`/api/logout`);
};

export const getBookList = (params) => {
  return request.get(`/api/books?${qs.stringify(params)}`);
};

export const bookUpdate = (id, params) => {
  return request.put(`/api/books/${id}`, params);
};

export const bookAdd = (params) => {
  return request.post("/api/books", params);
};

export const getBookDetail = (id) => {
  return request.get(`/api/books/${id}`);
};

export const bookDelete = (id) => {
  return request.delete(`/api/books/${id}`);
};
export const getCategoryList = (params) => {
  return request.get(`/api/categories?${qs.stringify(params)}`);
};
export const categoryAdd = (params) => {
  return request.post("/api/categories", params);
};

export const categoryUpdate = (id, params) => {
  return request.put(`/api/categories/${id}`, params);
};

export const categoryDelete = (id) => {
  return request.delete(`/api/categories/${id}`);
};
export const getBorrowList = (params) => {
  return request.get(`/api/borrows?${qs.stringify(params)}`);
};

export const getBorrowDetail = (id) => {
  return request.get(`/api/borrows/${id}`);
};

export const borrowAdd = (params) => {
  return request.post(`/api/borrows`, params);
};

export const borrowDelete = (id) => {
  return request.delete(`/api/borrows/${id}`);
};
export const borrowUpdate = (id, params) => {
  return request.post(`/api/borrows/${id}`, params);
};

export const borrowBack = (id) => {
  return request.put(`/api/borrows/back/${id}`);
};
export const getUserList = (params) => {
  return request.get(`/api/users?${qs.stringify(params)}`);
};
