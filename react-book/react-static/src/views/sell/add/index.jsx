import {
  borrowAdd,
  borrowUpdate,
  getBookList,
  getUserList,
} from "../../../api";
import { Button, Form, Select, message } from "antd";

import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";

import styles from "./index.module.scss";

const BorrowForm = ({ title, editData }) => {
  const [form] = Form.useForm();
  const router = useNavigate();
  const [userList, setUserList] = useState([]);
  const [bookList, setBookList] = useState([]);
  const [bookStock, setBookStock] = useState(0);

  useEffect(() => {
    getUserList().then((res) => {
      setUserList(res.data);
    });
    getBookList({ all: true }).then((res) => {
      setBookList(res.data);
    });
  }, []);

  useEffect(() => {
    form.setFieldsValue(editData);
  }, [editData, form]);

  const handleFinish = async (values) => {
    try {
      if (editData?._id) {
        await borrowUpdate(editData._id, values);
        message.success("编辑成功");
      } else {
        await borrowAdd(values);
        message.success("创建成功");
      }
      router("/sell");
    } catch (error) {
      console.error(error);
    }
  };

  const handleBookChange = (value, option) => {
    setBookStock(option.stock);
  };

  return (
    <>
      <Form
        className={styles.form}
        labelCol={{ span: 6 }}
        wrapperCol={{ span: 18 }}
        form={form}
        onFinish={handleFinish}
        autoComplete="off"
      >
        <Form.Item
          label="书籍名称"
          name="book"
          rules={[
            {
              required: true,
              message: "请输入名称",
            },
          ]}
        >
          <Select
            placeholder="请选择"
            showSearch
            optionFilterProp="label"
            onChange={handleBookChange}
            options={bookList.map((item) => ({
              label: item.name,
              value: item._id,
              stock: item.stock,
            }))}
          />
        </Form.Item>
        <Form.Item
          label="售卖用户"
          name="user"
          rules={[
            {
              required: true,
              message: "请输入名称",
            },
          ]}
        >
          <Select
            placeholder="请选择"
            showSearch
            optionFilterProp="label"
            options={userList.map((item) => ({
              label: item.name,
              value: item._id,
            }))}
          />
        </Form.Item>
        <Form.Item
          label="书籍库存"
          rules={[
            {
              required: true,
              message: "请输入名称",
            },
          ]}
        >
          {bookStock}
        </Form.Item>
        <Form.Item label=" " colon={false}>
          <Button
            type="primary"
            htmlType="submit"
            size="large"
            className={styles.btn}
            // 库存<=0并且不是编辑模式，不能点击
            disabled={bookStock <= 0 && !editData?._id}
          >
            {editData?._id ? "编辑" : "创建"}
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default BorrowForm;
