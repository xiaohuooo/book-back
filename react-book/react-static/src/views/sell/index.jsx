import {
  borrowDelete,
  getBookList,
  getBorrowList,
  getCategoryList,
} from "../../api";
import { ExclamationCircleFilled } from "@ant-design/icons";
import {
  Button,
  Col,
  Form,
  Modal,
  Row,
  Select,
  Space,
  Table,
  message,
} from "antd";
import dayjs from "dayjs";
import { useCallback, useEffect, useState } from "react";

import styles from "./index.module.scss";

const Option = Select.Option;

const COLUMNS = [
  {
    title: "书籍名称",
    dataIndex: "bookName",
    key: "bookName",
    ellipsis: true,
    width: 150,
  },
  {
    title: "书籍作者",
    dataIndex: "author",
    key: "author",
    ellipsis: true,
    width: 150,
  },
  {
    title: "售卖人",
    dataIndex: "borrowUser",
    key: "borrowUser",
    ellipsis: true,
    width: 150,
  },
  {
    title: "售卖时间",
    dataIndex: "borrowAt",
    key: "borrowAt",
    width: 200,
    render: (text) => dayjs(text).format("YYYY-MM-DD"),
  },
];

export default function Borrow() {
  const [form] = Form.useForm();
  const [list, setList] = useState([]);
  const [categoryList, setCategoryList] = useState([]);
  const [bookList, setBookList] = useState([]);
  const [total, setTotal] = useState(0);
  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 20,
    showSizeChanger: true,
  });

  const columns = [
    ...COLUMNS,
    {
      title: "操作",
      dataIndex: "",
      key: "action",
      render: (_, row) => (
        <Space>
          <Button
            type="link"
            block
            danger
            onClick={() => {
              handleDeleteModal(row._id);
            }}
          >
            删除
          </Button>
        </Space>
      ),
    },
  ];

  const fetchData = useCallback(
    (search) => {
      const { book, user, author, status } = search || {};
      getBorrowList({
        current: pagination.current,
        pageSize: pagination.pageSize,
        book,
        author,
        user,
        status,
      }).then((res) => {
        const data = res.data.map((item) => ({
          ...item,
          bookName: item.book.name,
          author: item.book.author,
          borrowUser: item.user.nickName,
        }));
        setList(data);
        setTotal(res.total);
      });
    },
    [pagination]
  );

  useEffect(() => {
    fetchData();
  }, [fetchData, pagination]);

  useEffect(() => {
    getCategoryList({ all: true }).then((res) => {
      setCategoryList(res.data);
    });
    getBookList({ all: true }).then((res) => {
      setBookList(res.data);
    });
  }, []);

  const handleDeleteModal = (id) => {
    Modal.confirm({
      title: "确认删除？",
      icon: <ExclamationCircleFilled />,
      okText: "确定",
      cancelText: "取消",
      async onOk() {
        await borrowDelete(id);
        fetchData(form.getFieldsValue());
        message.success("删除成功");
      },
    });
  };

  const handleTableChange = (pagination) => {
    setPagination(pagination);
  };

  const handleSearchFinish = (values) => {
    fetchData(values);
  };

  return (
    <>
      <Form
        form={form}
        name="search"
        className={styles.form}
        onFinish={handleSearchFinish}
      >
        <Row gutter={24}>
          <Col span={5}>
            <Form.Item name="book" label="书籍名称">
              <Select
                showSearch
                placeholder="请选择"
                optionFilterProp="label"
                allowClear
                options={bookList.map((item) => ({
                  label: item.name,
                  value: item._id,
                }))}
              />
            </Form.Item>
          </Col>
          <Col span={5}>
            <Form.Item name="status" label="状态">
              <Select
                showSearch
                placeholder="请选择"
                optionFilterProp="label"
                allowClear
                options={[
                  { label: "借出", value: "on" },
                  { label: "归还", value: "off" },
                ]}
              />
            </Form.Item>
          </Col>
          <Col span={5}>
            <Form.Item name="user" label="售卖人">
              <Select placeholder="请选择" allowClear>
                {categoryList.map((category) => (
                  <Option key={category._id} value={category._id}>
                    {category.name}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={9} style={{ textAlign: "left" }}>
            <Button type="primary" htmlType="submit">
              搜索
            </Button>
            <Button
              style={{ margin: "0 8px" }}
              onClick={() => {
                form.resetFields();
              }}
            >
              清空
            </Button>
          </Col>
        </Row>
      </Form>
      <div className={styles.tableWrap}>
        <Table
          rowKey="_id"
          dataSource={list}
          columns={columns}
          onChange={handleTableChange}
          scroll={{ x: 800 }}
          pagination={{
            ...pagination,
            total: total,
            showTotal: () => `共 ${total} 条`,
          }}
        />
      </div>
    </>
  );
}
