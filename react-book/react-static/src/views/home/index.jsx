import React from "react";
import Layout from "../../components/layout";
import "./index.scss";
import { Outlet } from "react-router-dom";
export default function index() {
  return (
    <div className="home">
      <Layout>
        <Outlet></Outlet>
      </Layout>
    </div>
  );
}
