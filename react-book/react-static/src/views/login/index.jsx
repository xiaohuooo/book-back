import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { Button, Checkbox, Form, Input } from "antd";
// 引入CryptoJS库
// import CryptoJS from "crypto-js";
import { useNavigate } from "react-router-dom";
import "./index.scss";
import { message } from "antd";
const App = () => {
  const router = useNavigate();
  const onFinish = (values) => {
    console.log("Received values of form: ", values);
    const { username, password } = values;
    // 对密码进行SHA256哈希处理
    // const hashedPassword = CryptoJS.SHA256(password).toString();
    if (username === "admin" && password === "123") {
      message.success("登陆成功");
      router("/book");
    } else {
      message.warning("账号密码错误");
    }
  };
  return (
    <div className="login">
      <h3>图书管理系统</h3>
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
      >
        <Form.Item
          name="username"
          rules={[
            {
              required: true,
              message: "请输入账号",
            },
          ]}
        >
          <Input
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Username"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: "请输入密码",
            },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
          />
        </Form.Item>
        <Form.Item>
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <Checkbox>记住密码</Checkbox>
          </Form.Item>
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
            style={{ width: "100%" }}
          >
            登录
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};
export default App;
