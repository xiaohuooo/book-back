const { createProxyMiddleware } = require("http-proxy-middleware");

module.exports = function (app) {
  app.use(
    createProxyMiddleware("/api", {
      target: "https://mock.apifox.cn/m1/2398938-0-default/",
      secure: false,
      changeOrigin: true,
    })
  );
};
