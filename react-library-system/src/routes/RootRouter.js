import Login from '../layout/LoginPage/Login'
import Test from '../test'
import App from '../layout/App'
import { Navigate, Route, Routes } from 'react-router-dom'
const RootRouter = () => {
    return (
      <Routes>
        <Route path="/test" element={<Test/>}></Route>
        <Route path="/login" element={<Login/>}></Route>
        <Route path="/" element={<Navigate to="/login" />}></Route>
        <Route path="/*" element={<App />}></Route>
      </Routes>
    )
  
  }
  export default  RootRouter;