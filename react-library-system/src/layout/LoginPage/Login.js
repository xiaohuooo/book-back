import {
  AlipayCircleOutlined,
  LockOutlined,
  MobileOutlined,
  TaobaoCircleOutlined,
  UserOutlined,
  WeiboCircleOutlined,
} from '@ant-design/icons';
import {
  LoginForm,
  ProConfigProvider,
  ProFormCaptcha,
  ProFormCheckbox,
  ProFormText,
} from '@ant-design/pro-components';
import { Button, message, Space, Tabs } from 'antd';
import { useEffect, useState } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import userStore from '../../stores/UserStore';
import { observer, useLocalObservable } from 'mobx-react-lite';
import { toJS } from 'mobx';

const Login= observer(()=>{
  const localUserStore = useLocalObservable(() => userStore);
  const navigate=useNavigate()
  const [message, setmessage] = useState(" ");
  // const handleLogin=(user)=>{

  // }
  const user_info=toJS(localUserStore.user_info)
  
  
  return (
    <ProConfigProvider hashed={false}>
      <div style={{ backgroundColor: 'white' }}>
        <LoginForm
          logo="https://img1.baidu.com/it/u=3561800453,140595587&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500"
          title="HznuLibrary"
          subTitle="全球最大的图书管理系统"
          onFinish={async (user)=>{
            console.log("点击登录"+user.user_id);
            // 同步
            let message=await localUserStore.login(user);
            setmessage(message);
            // navigate('/*',{state:{ user_name: user_info.user_name, user_id:user_info.user_id,user_type:user_info.user_type }});

          }}
        
         
          actions={
            <Space>
             
            </Space>
          }
        >
          
            
          {/* render的组件里必须有store的值才会刷新 */}
          {/* <Button onClick={()=>{
            localUserStore.add()
          }}>{localUserStore.count}</Button> */}
        <div style={{textAlign:'center',
              fontSize: "25px",
              padding:"10px"
            }}>
          登录
        </div>
            <>
              <ProFormText
                name="user_id"
                fieldProps={{
                  size: 'large',
                  prefix: <UserOutlined className={'prefixIcon'} />,
                }}
                placeholder={'学工号: 123'}
                rules={[
                  {
                    required: true,
                    message: '请输入学工号!',
                  },
                ]}
              />
              <ProFormText.Password
                name="password"
                fieldProps={{
                  size: 'large',
                  prefix: <LockOutlined className={'prefixIcon'} />,
                }}
                placeholder={'密码: 123456'}
                rules={[
                  {
                    required: true,
                    message: '请输入密码！',
                  },
                ]}
              />
            </>
          
          <div
            style={{
              marginBlockEnd: 24,
            }}
          >
            <ProFormCheckbox noStyle name="autoLogin">
              自动登录
            </ProFormCheckbox>
            <a
              style={{
                float: 'right',
              }}
            >
              忘记密码
            </a>
          </div>
          {JSON.stringify(user_info) !== '{}' ?<Navigate to="/*"></Navigate>:<div style={{textAlign:"center",color:"red"}}>{`${message}`}</div>}
          {/* {message=="SUCCESS"?<Navigate to="/*"></Navigate> :<div>{{message}}</div>}  */}
        </LoginForm>
      </div>
    </ProConfigProvider>
  );
});
export default Login;