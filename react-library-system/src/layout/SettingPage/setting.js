import {  Button, Checkbox, Form, Input, Space, message} from 'antd';
import {React,useState,useEffect} from 'react';
import userStore from '../../stores/UserStore';



const SettingPage = () => {
  const [messageApi,contextHolder]=message.useMessage();
  const user_id=sessionStorage.getItem("user_id");
  const user_name=sessionStorage.getItem("user_name");
  const phone=sessionStorage.getItem("phone");
  return (
    
    <Form
    name="basic"
    labelCol={{ span: 5 }}
    wrapperCol={{ span: 10 }}
    style={{ maxWidth: 600 }}
    initialValues={{ remember: true }}
    onFinish={(values)=>{
      console.log(values);
      console.log('Success:', values);
      // 修改个人信息
      userStore.modifyUser(values);

      messageApi.open({
        type: 'success',
        content: '修改成功',
      });
     
    }}
    onFinishFailed={()=>{

    }}
    autoComplete="off"
  >
    <Form.Item
      label="学工号"
      name="user_id"
      initialValue={user_id}
    >
      <Input  placeholder={user_id}  disabled />
    </Form.Item>

    <Form.Item
      label="姓名"
      name="user_name"
      initialValue={user_name}
      rules={[{ required: true, message: '请输入姓名' }]}
    >
      <Input  />
    </Form.Item>

    <Form.Item
      label="重置密码"
      name="password"
      
      rules={[{ required: true, message: '请输入密码' }]}
    >
      <Input type="password"/>
    </Form.Item>

    <Form.Item
      label="手机号"
      name="phone"
      initialValue={phone}
      rules={[{ required: true, message: '请输入手机号' }]}
    >
      <Input />
    </Form.Item>

    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
      <Button type="primary" htmlType="submit">
        提交
      </Button>
    </Form.Item>
    {contextHolder}
  </Form>
  );
};
export default SettingPage;
