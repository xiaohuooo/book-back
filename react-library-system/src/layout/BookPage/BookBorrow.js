import { } from '@ant-design/icons';
import { Breadcrumb, Layout, theme ,Dropdown, Space} from 'antd';
import {React} from 'react';
import { Button, Checkbox, Form, Input, message } from 'antd';
import bookStore from '../../stores/BookStore';
import { observer } from 'mobx-react-lite';


const  BookBorrow= () => {
  const user_type=sessionStorage.getItem("user_type");
  const user_id=sessionStorage.getItem("user_id");
  const [messageApi, contextHolder] = message.useMessage();
  // 是否被限制借书
  const status=1;
  const handleBorrow = async (reader_id,book_id) => {
    if(status){
      let borrow_date = new Date();
      let multiple = user_type == 2 ? 60 : 30;
      let due_date = new Date(borrow_date.getTime() + multiple*24*60*60*1000); 
      borrow_date = borrow_date.toISOString().split('T')[0];
      due_date = due_date.toISOString().split('T')[0];
      let resultMsg = await bookStore.borrowbook({reader_id,book_id,borrow_date,due_date});

      messageApi.open({
        type: 'success',
        content: resultMsg,
      });
    }
    else{
      messageApi.open({
        type: 'error',
        content: "账号被禁止借书",
      });
    }
    
   
   
  };
  // useEffect(() => {

  // }, [data]);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  // const [dataSource, setDataSource] = useState();
  
  // const navigate=useNavigate();
  return (
    <Form
    name="basic"
    labelCol={{ span: 5 }}
    wrapperCol={{ span: 10 }}
    style={{ maxWidth: 600 }}
    initialValues={{ remember: true }}
    onFinish={(values)=>{
        // console.log('Success:', values);
        console.log(values)
        handleBorrow(values.reader_id,values.book_id);
    }}
    onFinishFailed={()=>{

    }}
    autoComplete="off"
  >
    
      {user_type==1
      ?
        <Form.Item
          label="学工号"
          name="reader_id"
          type="int"
          rules={[{ required: false }]}
          // 默认值
          initialValue={user_id}
        >
          <Input type='int' />
      </Form.Item>
      :
      <Form.Item
          label="学工号"
          name="reader_id"
          type="int"
          rules={[{ required: false }]}
          // 默认值
          initialValue={user_id}
        >
           <Input type='int' placeholder={user_id} disabled />
      </Form.Item>
     }
      
    

    <Form.Item
      label="书籍编号"
      name="book_id"
      type="int"
      rules={[{ required: true, message: '请输入书籍编号' }]}
    >
      <Input />
    </Form.Item>


    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
      {/* 根据状态禁用按钮 */}
      <Button type="primary" htmlType="submit">
        借阅
      </Button>
      <>
        {contextHolder}
      </>

    </Form.Item>
  </Form>
  );
};
export default BookBorrow;
