import {SearchOutlined,ClearOutlined,CloseCircleOutlined } from '@ant-design/icons';
import {  Table, theme ,Dropdown, Space,Input,Button,Form,InputNumber,Typography,message, Select} from 'antd';
import {React,useState,useEffect,useRef} from 'react';
import {book_data} from '../../stores/constant'
import Highlighter from 'react-highlight-words';
import { toJS } from 'mobx';
import bookStore from '../../stores/BookStore'
import {  observer } from 'mobx-react-lite';

const BookList= observer(() => {
  const user_type=sessionStorage.getItem("user_type");

  // 表格item修改
  const [form] = Form.useForm();
  const [messageApi, contextHolder] = message.useMessage();
  // const [data, setData] = useState(originData);
  const [editingKey, setEditingKey] = useState('');
  const isEditing = (record) => record.key === editingKey;
  const EditableCell = ({
    editing,
    dataIndex,
    title,
    inputType,
    record,
    index,
    children,
    ...restProps
  }) => {
    const inputNode = inputType === 'category_name' 
    ?   
    <Select>
      <Select.Option value="文学">文学</Select.Option>
      <Select.Option value="历史">历史</Select.Option>
      <Select.Option value="艺术">艺术</Select.Option>
      <Select.Option value="科学">科学</Select.Option>
      <Select.Option value="哲学">哲学</Select.Option>
    </Select>
    : 
    <Input />;
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item
            name={dataIndex}
            style={{
              margin: 0,
            }}
            rules={[
              {
                required: true,
                message: `Please Input ${title}!`,
              },
            ]}
          >
            {inputNode}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  //表格item搜索
  const [searchText, setSearchText] = useState('');
  const [searchedColumn, setSearchedColumn] = useState('');
  const searchInput = useRef(null);
  const categoryList = ["文学","历史","艺术","科学","哲学"]
  // 对bookList的item添加key属性
  const bookList = toJS(bookStore.bookList).map((record)=>{
    record.key="book"+record.book_id;
    record.category_name=categoryList[record.category_id-1]
    return record;
});

  useEffect(()=>{

    // 获取初始化数据
    bookStore.getBookList();
  },[])
  
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText('');
  };
  const edit = (record) => {
    console.log(record);
    form.setFieldsValue({
      ...record,
    });
    setEditingKey(record.key);
  };
  const save = async (record) => {
    try {
      const row = await form.validateFields();
      let newRecord = row;
      newRecord.book_id=record.book_id;
      newRecord.category_id=categoryList.indexOf(newRecord.category_name)+1;
      bookStore.modifyBook(newRecord);
      setEditingKey('');
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
    messageApi.open({
      type: 'success',
      content: '修改成功',
    });
  };
  // 查询功能，给出参数列标(dataIndex)高亮数据
  const getColumnSearchProps = (dataIndex,title) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters, close }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`请输入`+title}
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: 'block',
          }}
        />
        <Space>
          <Button
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            size="small"
            type="primary"
            icon={<SearchOutlined />}
            style={{
              width: 90,
            }}
          >
            搜索
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            
            icon={<ClearOutlined />}
            style={{
              width: 90,
            }}
          >
            重置
          </Button>

          <Button
            danger
            icon={<CloseCircleOutlined />}
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
          >
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? '#1677ff' : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: '#ffc069',
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });
 const columns = [
      {
        title: '编号',
        dataIndex: 'book_id',
        key: 'book_id',
        render: (text) => <a>{text}</a>,
      },
   
      {
        title: '书名',
        dataIndex: 'book_name',
        key: 'book_name',
        width:"20%",
        editable: true,
        ...getColumnSearchProps('book_name','姓名'),
      },
      // {
      //   title: '类别',
      //   dataIndex: 'category_id',
      //   key: 'category_id',
      //   width:"10%",
      //   editable: true,
      // },
      {
        title: '类别',
        dataIndex: 'category_name',
        key: 'category_name',
        width:"10%",
        editable: true,
        inputType:"category_name"
      },
      {
        title: '作者',
        dataIndex: 'author',
        key: 'author',
        width:"10%",
        editable: true,
        ...getColumnSearchProps('author','作者'),
        
      },
    
      {
        title: '状态',
        dataIndex: 'status',
        key: 'status',
        width:"12%",
        editable: true,
      },
    
      {
        title: '操作',
        dataIndex: 'operation',
        key: 'operation',
        render: (_,record) => {
          const editable = isEditing(record);
          return (
            <>
              <Button 
                danger  
                disabled={user_type!=1} 
                onClick={()=>{
                  console.log("删除"+record.book_id);
                  bookStore.deleteBook(record.book_id);
                  messageApi.open({
                    type: 'success',
                    content: '删除成功',
                  });
                }}>
                删除
              </Button>
         
            <span>  </span>

            { editable 
            ? 
              <span>
                <Button
                  onClick={() => save(record)}
                  style={{
                    marginRight: 8,
                  }}
                >
                  保存
                </Button>
              </span>
            : 
            
              <Button type='primary' disabled={(editingKey != '') | (user_type!=1)} onClick={() => edit(record)}>
                修改
              </Button>
            }
            </>
          )
        }
      },       
  ];

  const mergedColumns = columns.map((col) => {
   
    if (!col.editable) {
      return col;
    }
    else{
      return {
        ...col,
        onCell: (record) => ({
          record,
          inputType: col.dataIndex === 'category_name' ? 'category_name' : 'text',
          dataIndex: col.dataIndex,
          title: col.title,
          editing: isEditing(record),
        }),
      };
    }
    
  });


  return (
    <>
    {/* <Button onClick={()=>{
      bookStore.add();
    }}>{bookStore.count}</Button> */}
      <Form form={form} component={false}>
        <Table  
          rowClassName="editable-row"
          components={{
            body: {
              cell: EditableCell,
            },
          }} 
          dataSource={bookList} 
          columns={mergedColumns} 
          pagination={{pageSize:7}}/>
      </Form>
      <>
        {contextHolder}
      </>
    </>
  
  
  );
});
export default BookList;