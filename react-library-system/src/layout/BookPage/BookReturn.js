import { SearchOutlined,ClearOutlined,CloseCircleOutlined } from '@ant-design/icons';
import {Modal, Table, theme, Space,Input,Button,Form,message,InputNumber,} from 'antd';
import {React,useRef,useState,useEffect} from 'react';
import {borrow_data} from '../../stores/constant'
import Highlighter from 'react-highlight-words';
import bookStore from '../../stores/BookStore';
import { toJS } from 'mobx';
import { observer } from 'mobx-react-lite';


const BookReturn= observer(() => {
  const user_id=sessionStorage.getItem("user_id");
  // 读者界面显示个人借阅信息,显示还书按钮
  //图书管理员显示所有借阅信息,显示还书+修改按钮
  const reader_type=sessionStorage.getItem("user_type");
  const [editingRecord,setEditingRecord]=useState({});

   // 表格item修改
   const [form] = Form.useForm();
   const [messageApi, contextHolder] = message.useMessage();
   // const [data, setData] = useState(originData);
   const [editingKey, setEditingKey] = useState('');
   const isEditing = (record) => record.key === editingKey;
   const EditableCell = ({
     editing,
     dataIndex,
     title,
     inputType,
     record,
     index,
     children,
     ...restProps
   }) => {
     const inputNode = inputType === 'number' ? <InputNumber /> : <Input />;
     return (
       <td {...restProps}>
         {editing ? (
           <Form.Item
             name={dataIndex}
             style={{
               margin: 0,
             }}
             rules={[
               {
                 required: true,
                 message: `Please Input ${title}!`,
               },
             ]}
           >
             {inputNode}
           </Form.Item>
         ) : (
           children
         )}
       </td>
     );
   };

  const borrowList = toJS(bookStore.borrowList).map((record)=>{
    record.key="borrow"+record.borrow_id;
    record.type= reader_type===2 ?"老师":"学生";
    return record;
  });
  useEffect(() => {
    if(reader_type==1)
      bookStore.getBorrowList();
    else 
      bookStore.getBorrowById(user_id);

  }, [editingRecord]);
  
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [searchedColumn, setSearchedColumn] = useState('');
  const searchInput = useRef(null);
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText('');
  };
  
  const edit = (record) => {
    form.setFieldsValue({
      ...record,
    });
    setEditingKey(record.key);
  };
  const save = async (record) => {
    try {
      // row只有可修改的key
      const row = await form.validateFields();
      let newRecord = record;
      newRecord.borrow_date=row.borrow_date;
      newRecord.return_date=row.return_date;
      newRecord.fine=row.fine;
      bookStore.modifyBorrow(newRecord);
      setEditingKey('');
    
      messageApi.open({
        type: 'success',
        content: '修改成功',
      });
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };
  // 查询功能，给出参数列标(dataIndex)高亮数据
  const getColumnSearchProps = (dataIndex,title) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters, close }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`请输入`+title}
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: 'block',
          }}
        />
        <Space>
          <Button
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            size="small"
            type="primary"
            icon={<SearchOutlined />}
            style={{
              width: 90,
            }}
          >
            搜索
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            
            icon={<ClearOutlined />}
            style={{
              width: 90,
            }}
          >
            重置
          </Button>

          <Button
            danger
            icon={<CloseCircleOutlined />}
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
          >
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? '#1677ff' : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: '#ffc069',
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

  const columns = [
    {
      title: '借阅编号',
      dataIndex: 'borrow_id',
      key: 'borrow_id',

      render: (text) => <a>{text}</a>,
    },
 
    {
      title: '书籍编号',
      dataIndex: 'book_id',
      key: 'book_id',
      // editable:true,
      ...getColumnSearchProps('book_id','书籍编号'),
    },
    {
      title: '读者学工号',
      dataIndex: 'reader_id',
      key: 'reader_id',
      // editable:true,
      ...getColumnSearchProps('reader_id','学工号'),
    },
    // {
    //   title: '身份',
    //   dataIndex: 'type',
    //   key: 'type',
    // },
    {
      title: '借书日期',
      dataIndex: 'borrow_date',
      key: 'borrow_date',
      editable:true,
      width:"13%"
    },
    {
      title: '应还日期',
      dataIndex: 'due_date',
      key: 'due_date',
      editable:true,
      width:"13%"
    },
    {
      title: '还书日期',
      dataIndex: 'return_date',
      key: 'return_date',
    },
  
    // {
    //   title: '状态',
    //   dataIndex: 'status',
    //   key: 'status',
    // },
    // {
    //   title: '是否逾期',
    //   dataIndex: 'overdue',
    //   key: 'overdue',
    // },
    {
      title: '罚金',
      dataIndex: 'fine',
      key: 'fine',
      editable:true,
      width:"8%"
    },
  
    {
      title: '操作',
      dataIndex: 'operation',
      key: 'operation',
      render: (_,record) => {
        const editable = isEditing(record);
        return(
        <>
          <Button type="primary"
            disabled={record.return_date!==null}
            onClick={()=>{
              let borrow=record;
              // borrow.return_date=new Date().toISOString().split('T')[0];
              // 计算罚金
              var endTime=new Date().getTime();
              var starTime=new Date(borrow.due_date).getTime();
              if(endTime>starTime) {
                  var diff=parseInt((endTime-starTime)/(1000 * 60 * 60 * 24));
                  borrow.diff=diff;
                  console.log("逾期天数"+diff);
                  borrow.fine=(0.1*diff).toFixed(1);
              }
              else{
                borrow.fine=0;
              }
              setEditingRecord(borrow);
              setIsModalOpen(true);
            
            }}>
            还书
          </Button>

          <span>   </span>
          { editable 
        ? 
          <span>
            <Button
              onClick={() => save(record)}
              style={{
                marginRight: 8,
              }}
            >
              保存
            </Button>
          </span>
        : 
        
          <Button type='primary' disabled={editingKey !== ''} onClick={() => edit(record)}>
            修改
          </Button>
        }

        </>
      )
      },
    }
  
  ];

  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record) => ({
        record,
        inputType: col.dataIndex === 'age' ? 'number' : 'text',
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });



  return (
    <>
    <Form form={form} component={false}>
      <Table   rowClassName="editable-row"
          components={{
            body: {
              cell: EditableCell,
            },
          }}  dataSource={borrowList} columns={mergedColumns} pagination={{pageSize:7}}/>
        <Modal title="还书提醒" open={isModalOpen} 
          okText="确认还书"
          cancelText="取消"
          onOk={()=>{
            setIsModalOpen(false);
            // 还书
            let borrow=editingRecord;
            borrow.return_date=new Date().toISOString().split('T')[0];
            bookStore.returnBook(editingRecord);
            messageApi.open({
              type: 'success',
              content: '还书成功',
            });
        }} 
          onCancel={()=>{
            setIsModalOpen(false);
        }} 
      >
      <p>
        {/* 是否确认还书？ */}
      </p>
      <h3 style={{color:"red"}}>
        {/* {Object.values(editingRecord).map((record)=>{
          return <p>{`${record}`}</p>
        })} */}
        {editingRecord.fine!==0.0 ?`已逾期`+editingRecord.diff+`天 需缴纳`+editingRecord.fine+`元罚金`:""}
      </h3>
      </Modal>
      </Form>
      {contextHolder}
    </>
    
  
  );
});
export default BookReturn;