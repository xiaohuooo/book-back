import React, { useEffect, useState } from 'react';
import './book.css'
import { book_data } from '../../stores/constant';
import  * as echarts from 'echarts'
import bookStore from '../../stores/BookStore';
import { toJS,reaction} from 'mobx';
import { observer, useLocalObservable } from 'mobx-react-lite';
import { Button } from 'antd';

const BookClassify=observer(()=>{

  const pieRef = React.useRef(null);
  const [isLoading,setisLoading]=useState(false);
  useEffect(()=>{
    bookStore.classfyBook();
    
    const pie=echarts.init(pieRef.current);
    const option= {
          title: {
              text: '书籍分类',
              top: 30,
              
              left: 'center'
            },
            tooltip: {
              trigger: 'item'
            },
            legend: {
              orient: 'vertical',
              left: 'left'
            },
            series: [
              {
                name: '书籍数',
                type: 'pie',
                radius: '50%',
                // data: [
                //   { value:  9, name: '文学' },
                //   { value: 10, name: '历史' },
                //   { value: 7, name: '艺术' },
                //   { value: 2, name: '哲学' },
                //   { value: 3, name: '科学' }
                // ],
                data:bookStore.filterCategory,
                label:{
                  formatter: '{b} {d}%  ',
                },
                
                emphasis: {
                  itemStyle: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                  }
                }
              }
            ]
        };
    pie.setOption(option);

  
    return () => {
      pie.dispose();
      // dispose();

    };
},[isLoading])


 
  
    
    return (
      <>
      <div id="bookclassfiy-pie" ref={pieRef} ></div>
      {/* 被动刷新数据 */}
      <div>
        <Button 
          className="reload-btn"
          type='primary'
          onClick={()=>{
            // bookStore.classfyBook();
            setisLoading(!isLoading);
          }}>
          刷新数据
        </Button>
      {/* {bookCategory.length ?<p>123</p> :<p>...</p>} */}
      </div>
      
      </>
    )
});

export default BookClassify;