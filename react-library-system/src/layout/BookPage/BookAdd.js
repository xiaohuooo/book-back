import { DownOutlined, UserOutlined,SettingOutlined,SmileOutlined } from '@ant-design/icons';
import { Breadcrumb, Layout, Menu, Table, theme ,Dropdown, Space} from 'antd';
import {React,useState,useEffect} from 'react';
import { Button, Checkbox, Form, Input,Select,message } from 'antd';
import bookStore from '../../stores/BookStore';

const { Header, Content, Footer, Sider } = Layout;


const BookAdd= () => {
  // useEffect(() => {

  // }, [data]);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const [messageApi, contextHolder] = message.useMessage();
  return (
    <>
    {contextHolder}
    <Form
    name="basic"
    labelCol={{ span: 5 }}
    wrapperCol={{ span: 10 }}
    style={{ maxWidth: 600 }}
    initialValues={{ remember: true }}
    onFinish={(book)=>{
        console.log('Success:', book);
        let result=bookStore.addBook(book);
        if(result){
          messageApi.open({
            type: 'success',
            content: '添加成功',
          });
        }
        else{
          messageApi.open({
            type: 'error',
            content: '添加失败',
          });
        }
    }}
    onFinishFailed={()=>{

    }}
    autoComplete="off"
  >
    <Form.Item
      label="编号"
      name="book_id"
      rules={[{ required: true, message: '请输入编号' }]}
    >
      <Input />
    </Form.Item>

    <Form.Item
      label="书名"
      name="book_name"
      rules={[{ required: true, message: '请输入书籍名称' }]}
    >
      <Input />
    </Form.Item>

    <Form.Item
      label="分类"
      name="category_id"
      rules={[{ required: true, message: '请输入类别' }]}
    >
        <Select>
          <Select.Option value="1">文学</Select.Option>
          <Select.Option value="2">历史</Select.Option>
          <Select.Option value="3">艺术</Select.Option>
          <Select.Option value="4">科学</Select.Option>
          <Select.Option value="5">哲学</Select.Option>
        </Select>
    </Form.Item>

    <Form.Item
      label="作者"
      name="author"
      rules={[{ required: true, message: '请输入作者名' }]}
    >
      <Input />
    </Form.Item>

    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
      <Button type="primary" htmlType="submit">
        提交
      </Button>
    </Form.Item>
  </Form>
  </>
  
  );
};
export default BookAdd;
