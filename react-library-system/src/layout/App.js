import { DownOutlined, UserOutlined,SettingOutlined,BookOutlined,CreditCardOutlined } from '@ant-design/icons';
import { Breadcrumb, Layout, Menu, Table, theme ,Dropdown, Space} from 'antd';
import {React,useState,useEffect} from 'react';

import {useNavigate,Route,Routes,Link, Outlet,useLocation} from 'react-router-dom'
import './App.css'

import BookList from './BookPage/BookList';
import BookAdd from './BookPage/BookAdd';
import AdminList from './UserPage/AdminPage/AdminList';
import AdminAdd from './UserPage/AdminPage/AdminAdd';
import ReaderList from './UserPage/ReaderPage/ReaderList';
import ReaderAdd from './UserPage/ReaderPage/ReaderAdd';
import BookBorrow from './BookPage/BookBorrow';
import BookReturn from './BookPage/BookReturn';
import SettingPage from './SettingPage/setting';
import BookClassify from './BookPage/BookClassify';
const { Header, Content, Footer, Sider } = Layout;



const App = () => {
  // useEffect(() => {

  // }, [data]);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const user_id = parseInt(sessionStorage.getItem("user_id"));
  const user_name= sessionStorage.getItem("user_name");
  const user_type= parseInt(sessionStorage.getItem("user_type"));
  console.log(user_name, user_id,user_type);
  const navigate=useNavigate();
  // const user_name="刘浏",user_type=0;

  const USER_ITEMS = [
    {
      key: '1',
      label: (
        <a target="_blank" rel="noopener noreferrer" onClick={()=>{
          sessionStorage.clear();
          navigate("/login");
        }}href="/login">
          退出
        </a>
      ),
    },
  
  ];
  const items=[ 
    {
    key: `book`,
    icon: <BookOutlined />,
    label: `图书管理`,
    children: [
      {
        key: "book/add",
        label: `书籍添加`,
        disabled:user_type!==1
      },
      {
        key: "book/list",
        label: `书籍列表`,
        
      },
      {
        key: "book/category",
        label: `书籍分类`,
        disabled:user_type!==1
      },
    ]
    },
    {
      key: `user`,
      icon: <UserOutlined/>,
      label: `用户管理`,
      children: [
        {
          key: "user/admin/",
          label: `图书管理员管理`,
          disabled:user_type!==0,
          children: [
            {
              key: "user/admin/add",
              label: `管理员添加`,
            },
            {
              key:"user/admin/list",
              label:`管理员列表`,
            }
        ]
        },
        {
          key: "user/reader",
          label: `读者管理`,
          disabled:user_type!==1,
          children: [
            {
              key: "user/reader/add",
              label: `读者添加`,
            },
            {
              key:"user/reader/list",
              label:`读者列表`,
            }
        ]
        },
        // {
        //   key: "6",
        //   label: `读者管理`,
        // },
      ]
      },
      {
        key: `circulate`,
        icon: <CreditCardOutlined />,
        label: `借阅管理`,
        disabled:user_type===0,
        children: [
          {
            key: "circulate/borrow",
            label: `借书`,
          },
          {
            key: "circulate/return ",
            label: `还书`,
          },
  
        ]
        },
    {
      key: `setting`,
      icon: <SettingOutlined />,
      label: `设置`,
      // disabled: true,
    }
  ]
  return (
    <Layout>
      <Header
      className="logo-title"
      >
      <div style={{display:"flex" ,alignItems:"center"}}>
        <svg t="1687763734943" className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2308" width="32" height="32"><path d="M511.3 191.8H94c-16.4 0-29.8 13.3-29.8 29.7l-0.8 644.3c0 16.5 13.3 29.8 29.8 29.8h306.1l8.3 16.3c5.1 10 15.3 16.3 26.6 16.3h156c10.9 0 20.9-6 26.2-15.5l9.3-17h304c16.4 0 29.8-13.3 29.8-29.8V221.6c0-16.4-13.3-29.8-29.8-29.8H511.3z" fill="#08A0F0" p-id="2309"></path><path d="M517.8 189.4c-5.3 3.6-12.1 4.3-18 1.7-32.7-14.4-143.8-59.4-248.8-59.4H144.5c-10.2 0-18.5 8.3-18.5 18.5v666.6c0 10.2 8.3 18.5 18.5 18.5h328.3c5.4 0 10.5 2.3 14 6.4l8.8 10.1c7.3 8.4 20.3 8.5 27.7 0.3l9.8-10.7c3.5-3.9 8.5-6.1 13.7-6.1h328.8c10.2 0 18.5-8.3 18.5-18.5l0.9-666.6c0-10.3-8.3-18.6-18.5-18.6H764.2c0 0.1-155.6-4.1-246.4 57.8z" fill="#FFFFFF" p-id="2310"></path><path d="M511 894.1l-31.3-36c-3.5-4.1-8.6-6.4-14-6.4H126.6c-10.2 0-18.5-8.3-18.5-18.5V126.6c0-10.2 8.3-18.5 18.5-18.5H253c100.6 0 204.6 39 247.2 56.9 5.6 2.3 11.9 1.8 17-1.3 94.5-58.4 239.7-55.8 249.4-55.6h132.1c10.3 0 18.6 8.3 18.5 18.6l-1 706.6c0 10.2-8.3 18.5-18.5 18.5h-340c-5.2 0-10.2 2.2-13.7 6.1l-33 36.2z m-363-82.4h336c5.4 0 10.5 2.3 14 6.4 7.3 8.4 20.3 8.5 27.7 0.3l0.6-0.6c3.5-3.9 8.5-6.1 13.7-6.1h336.2l0.9-663.7H765.7c-1.6 0-159.7-3.5-242.2 59.7l-0.7 0.5c-5.5 4.2-13 5-19.3 2l-0.8-0.4C501 209 371.5 148 252.9 148H148v663.7z" fill="#8A8A8A" p-id="2311"></path><path d="M511.3 200.1v667.5" fill="#FFFFFF" p-id="2312"></path><path d="M491.3 200.1h40v667.5h-40z" fill="#8A8A8A" p-id="2313"></path><path d="M576 291.6h256v40H576z m0 151.5h256v40H576z m0 168.4h256v40H576z m-385-320h256v40H191z m0 151.6h256v40H191z m0 168.4h256v40H191z" fill="#CDCDCD" p-id="2314"></path></svg>
        <h2>HZNU图书管理系统</h2>
      </div>
      
       <span>
        <Dropdown
          menu={{
            items:USER_ITEMS,
          }}
        >
          <a onClick={(e) => e.preventDefault()}>
            <Space>
              <UserOutlined />
              {user_name}
              <DownOutlined />
            </Space>
          </a>
        </Dropdown>
      </span>
       
       
      </Header>
      <Content
        style={{
          padding: '0 30px',
        }}
      >
       
        <Layout
          style={{
            padding: '24px 0',
            background: colorBgContainer,
          }}
        >
          <Sider
            style={{
              background: colorBgContainer,
            }}
            width={200}
          >
            <Menu
              mode="inline"
              defaultSelectedKeys={['1']}
              defaultOpenKeys={['sub1']}
              style={{
                height: '100%',
              }}
              items={items}
              onClick={({key})=>{
                navigate(key);
              }}
            />
          </Sider>
          <Content
            className='main-content'
          >
            
            <Routes>
              {/* <Route path="/book" element={<BookList/>} > */}
              <Route path="/book"  >
                <Route path="list" element={<BookList/>}/>
                <Route path="add"  element={<BookAdd/>} />
                <Route path="category"  element={<BookClassify/>} />
              </Route>

              <Route path="/user" >
                <Route path="admin" >
                  <Route path="list"  element={<AdminList/>} />
                  <Route path="add"  element={<AdminAdd/>} />
                </Route>
                <Route path="reader"  >
                  <Route path="list"  element={<ReaderList/>} />
                  <Route path="add"  element={<ReaderAdd/>} />
                </Route>
                </Route>


              <Route path="/circulate" >
                <Route path="borrow" element={<BookBorrow/>}/>
                <Route path="return"  element={<BookReturn/>} />
              </Route>

              <Route path="/setting"element={<SettingPage/>} />
              
            </Routes>
          <Outlet></Outlet>
          
          </Content>
        </Layout>
      </Content>
      <Footer
        style={{
          textAlign: 'center',
        }}
      >
        HZNU Library  ©2023 
      </Footer>
    </Layout>
  );
};
export default App;