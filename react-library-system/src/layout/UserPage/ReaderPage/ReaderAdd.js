import { DownOutlined, UserOutlined,SettingOutlined,SmileOutlined } from '@ant-design/icons';
import { Breadcrumb, Layout, Menu, Table, theme ,Dropdown, Space} from 'antd';
import {React,useState,useEffect} from 'react';
import { Button, Checkbox, Form, Input,Select ,message} from 'antd';
import bookStore from '../../../stores/BookStore';
import userStore from '../../../stores/UserStore';

const { Header, Content, Footer, Sider } = Layout;


const ReaderAdd= () => {
  const [messageApi, contextHolder] = message.useMessage();
  return (
    <Form
    name="basic"
    labelCol={{ span: 5 }}
    wrapperCol={{ span: 10 }}
    style={{ maxWidth: 600 }}
    initialValues={{ remember: true }}
    onFinish={(values)=>{
        console.log('Success:', values);
        userStore.addReader(values);
        let user={user_id:values.reader_id,user_name:values.reader_name,password:values.password,user_type:values.user_type,phone:values.phone}
        userStore.addUser(user);
        messageApi.open({
          type: 'success',
          content: '添加成功',
        });
    }}
    onFinishFailed={()=>{
      messageApi.open({
        type: 'error',
        content: '添加失败',
      });
    }}
    autoComplete="off"
  >
     <Form.Item
      label="学工号"
      name="reader_id"
      rules={[{ required: true, message: '请输入学工号' }]}
    >
      <Input />
    </Form.Item>

    <Form.Item
      label="姓名"
      name="reader_name"
      rules={[{ required: true, message: '请输入姓名' }]}
    >
      <Input />
    </Form.Item>

    <Form.Item
      label="身份"
      name="user_type"
      rules={[{ required: true, message: '请输入身份' }]}
    >
      <Select>
          <Select.Option value="2">老师</Select.Option>
          <Select.Option value="3">学生</Select.Option>
        </Select>
    </Form.Item>

    <Form.Item
      label="密码"
      name="password"
      rules={[{ required: true, message: '请输入密码' }]}
    >
      <Input type='password' />
    </Form.Item>

    <Form.Item
      label="性别"
      name="sex"
      rules={[{ required: true, message: '请输入性别' }]}
    >
      <Select>
          <Select.Option value="1">男</Select.Option>
          <Select.Option value="0">女</Select.Option>
      </Select>
        
    </Form.Item>

    <Form.Item
      label="年龄"
      name="age"
      rules={[{ required: true, message: '请输入年龄' }]}
    >
      <Input />
    </Form.Item>

    <Form.Item
      label="部门"
      name="dept"
      rules={[{ required: true, message: '请输入部门' }]}
    >
      <Input />
    </Form.Item>

    <Form.Item
      label="手机号"
      name="phone"
      rules={[{ required: true, message: '请输入手机号' }]}
    >
      <Input />
    </Form.Item>

    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
      <Button type="primary" htmlType="submit">
        提交
      </Button>
    </Form.Item>
    {contextHolder}
  </Form>
  
  );
};
export default ReaderAdd;
