import {SearchOutlined,ClearOutlined,CloseCircleOutlined } from '@ant-design/icons';
import { Layout, Table, theme , Space,Input,Button,InputNumber,Form,message} from 'antd';
import {React,useState,useEffect,useRef} from 'react';
import {toJS} from 'mobx'
import bookStore from '../../../stores/BookStore';
import Highlighter from 'react-highlight-words';
import { observer } from 'mobx-react-lite';
import userStore from '../../../stores/UserStore';

const { Header, Content, Footer, Sider } = Layout;

const  AdminList= observer(() => {

  const adminList=toJS(userStore.adminList).map((record)=>{
    record.key="employee_id"+record.employee_id;
    return record;
  });
  useEffect(() => {
    userStore.getAdminList();
  }, []);
  
  const [form] = Form.useForm();
  const [messageApi, contextHolder] = message.useMessage();
  const [editingKey, setEditingKey] = useState('');
  const isEditing = (record) => record.key === editingKey;
  const EditableCell = ({
    editing,
    dataIndex,
    title,
    inputType,
    record,
    index,
    children,
    ...restProps
  }) => {
    const inputNode = inputType === 'number' ? <InputNumber /> : <Input />;
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item
            name={dataIndex}
            style={{
              margin: 0,
            }}
            rules={[
              {
                required: true,
                message: `Please Input ${title}!`,
              },
            ]}
          >
            {inputNode}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  const [searchText, setSearchText] = useState('');
  const [searchedColumn, setSearchedColumn] = useState('');
  const searchInput = useRef(null);
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText('');
  };
  const edit = (record) => {
    console.log(record);
    form.setFieldsValue({
      ...record,
    });
    setEditingKey(record.key);
  };
  const save = async (record) => {
    try {
      console.log(record);
      const row = await form.validateFields();
      let newRecord = row;
      console.log(newRecord);
      newRecord.employee_id=record.employee_id;
      userStore.modifyLibrarian(newRecord);
      setEditingKey('');

      messageApi.open({
        type: 'success',
        content: '修改成功',
      });
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };
  // 查询功能，给出参数列标(dataIndex)高亮数据
  const getColumnSearchProps = (dataIndex,title) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters, close }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`请输入`+title}
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: 'block',
          }}
        />
        <Space>
          <Button
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            size="small"
            type="primary"
            icon={<SearchOutlined />}
            style={{
              width: 90,
            }}
          >
            搜索
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            
            icon={<ClearOutlined />}
            style={{
              width: 90,
            }}
          >
            重置
          </Button>

          <Button
            danger
            icon={<CloseCircleOutlined />}
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
          >
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? '#1677ff' : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: '#ffc069',
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

 const columns = [
  {
    title: '学工号',
    dataIndex: 'employee_id',
    key: 'employee_id',
    ...getColumnSearchProps('employee_name','学工号'),
    render: (text) => <a>{text}</a>,
  },

  {
    title: '姓名',
    dataIndex: 'employee_name',
    key: 'employee_name',
    width:"12%",
    editable: true,
    ...getColumnSearchProps('employee_name','姓名'),
  },


  {
      title: '手机号',
      dataIndex: 'phone',
      key: 'phone',
      width:"18%",
      editable: true,
  },

  {
    title: '操作',
    dataIndex: 'operation',
    key: 'operation',
    render: (_,record) => {
      const editable = isEditing(record);
      return (
        <>
          <Button 
            danger  
            onClick={()=>{
              console.log("删除"+record.employee_id);
              userStore.deleteLibrarian(record.employee_id);
              messageApi.open({
                type: 'success',
                content: '删除成功',
              });
            }}>
            删除
          </Button>
     
        <span>  </span>

        { editable 
        ? 
          <span>
            <Button
              onClick={() => save(record)}
              style={{
                marginRight: 8,
              }}
            >
              保存
            </Button>
          </span>
        : 
        
          <Button type='primary' disabled={editingKey !== ''} onClick={() => edit(record)}>
            修改
          </Button>
        }
        </>
      )
    }
      
  },
 

];

    const mergedColumns = columns.map((col) => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: (record) => ({
          record,
          inputType: col.dataIndex === 'age' ? 'number' : 'text',
          dataIndex: col.dataIndex,
          title: col.title,
          editing: isEditing(record),
        }),
      };
    });
  
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  // const [dataSource, setDataSource] = useState();
  
  // const navigate=useNavigate();
  return (
    <>
    <Form form={form} component={false}>
    <Table   
      rowClassName="editable-row"
      components={{
        body: {
          cell: EditableCell,
        },
      }}  
      dataSource={adminList} 
      columns={mergedColumns} 
      pagination={{pageSize:7}}/>
    </Form>
    {contextHolder}
    </>
  );
});
export default AdminList;