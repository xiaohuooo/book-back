import {  Layout, theme } from 'antd';
import {React,useState,useEffect} from 'react';
import { Button, Form, Input, message} from 'antd';
import userStore from '../../../stores/UserStore';

const { Header, Content, Footer, Sider } = Layout;


const AdminAdd= () => {
  // useEffect(() => {

  // }, [data]);
  const [messageApi, contextHolder] = message.useMessage();

  return (
    <Form
    name="basic"
    labelCol={{ span: 5 }}
    wrapperCol={{ span: 10 }}
    style={{ maxWidth: 600 }}
    initialValues={{ remember: true }}
    onFinish={(values)=>{
        console.log('Success:', values);
        userStore.addLibrarian(values);
        let user={user_id:values.employee_id,user_name:values.employee_name,password:values.password,user_type:1,phone:values.phone}
        userStore.addUser(user);
        messageApi.open({
          type: 'success',
          content: '添加成功',
        });
    }}
    onFinishFailed={()=>{
      messageApi.open({
        type: 'error',
        content: '修改失败',
      });

    }}
    autoComplete="off"
  >
    <Form.Item
      label="学工号"
      name="employee_id"
      rules={[{ required: true, message: '请输入学工号' }]}
    >
      <Input />
    </Form.Item>

    <Form.Item
      label="姓名"
      name="employee_name"
      rules={[{ required: true, message: '请输入姓名' }]}
    >
      <Input />
    </Form.Item>

    <Form.Item
      label="密码"
      name="password"
      rules={[{ required: true, message: '请输入密码' }]}
    >
      <Input type='password' />
    </Form.Item>

    <Form.Item
      label="手机号"
      name="phone"
      rules={[{ required: true, message: '请输入手机号' }]}
    >
      <Input />
    </Form.Item>

    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
      <Button type="primary" htmlType="submit">
        提交
      </Button>
    </Form.Item>
    {contextHolder}
  </Form>
  );
};
export default AdminAdd;


// const columns = [
//   {
//     title: '学工号',
//     dataIndex: 'employee_id',
//     key: 'employee_id',
//     ...getColumnSearchProps('employee_name','学工号'),
//     render: (text) => <a>{text}</a>,
//   },

//   {
//     title: '姓名',
//     dataIndex: 'employee_name',
//     key: 'employee_name',
//     ...getColumnSearchProps('employee_name','姓名'),
//   },


//   {
//       title: '手机号',
//       dataIndex: 'phone',
//       key: 'phone',
//   },

//   {
//     title: '操作',
//     dataIndex: 'operation',
//     key: 'operation',
//     render: (_,record) => {
//       const editable = isEditing(record);
//       return (
//         <>
//           <Button 
//             danger  
//             onClick={()=>{
//               console.log("删除"+record.reader_id);
//               userStore.deleteReader(record.reader_id);
//               messageApi.open({
//                 type: 'success',
//                 content: '删除成功',
//               });
//             }}>
//             删除
//           </Button>
     
//         <span>  </span>

//         { editable 
//         ? 
//           <span>
//             <Button
//               onClick={() => save(record)}
//               style={{
//                 marginRight: 8,
//               }}
//             >
//               保存
//             </Button>
//           </span>
//         : 
        
//           <Button type='primary' disabled={editingKey !== ''} onClick={() => edit(record)}>
//             修改
//           </Button>
//         }
//         </>
//       )
//     }
      
//   },
 

// ];