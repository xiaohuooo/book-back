import { observable, action,makeAutoObservable } from 'mobx';

class UserStore{
    isloading=true;     
    user_info={}
    userList=[]
    readerList=[]
    adminList=[]


     constructor(){
        makeAutoObservable(this);
     }


     login=(user)=>{
        // 先返回result后fetch
        let data=fetch('/login',{
            method:'post',
            headers: {
                'Content-Type': 'application/json',
              },
            body:JSON.stringify(user)
        }).then((res)=>{return res.json();
        }).then((data)=>{
            console.log(data);
            if(data.data){
                this.user_info=data.data;
                sessionStorage.setItem("user_id",this.user_info.user_id);
                sessionStorage.setItem("user_name",this.user_info.user_name);
                sessionStorage.setItem("user_type",this.user_info.user_type);
                sessionStorage.setItem("phone",this.user_info.phone);
                return data.result;
            }
            else{
                // 报错信息
                return data.message;

            }
        }).catch((error)=>{
            console.log(error)
            return null;
        })
        
        return data;

     }


    getReaderList=()=>{
        fetch('/reader/list')
        .then(res => {
           return res.json();
        })
        .then(data=>{
            console.log(data);
            this.readerList=data.data;
          
        })
    }

    getAdminList=()=>{
        fetch('/librarian/list')
        .then(res => {
           return res.json();
        })
        .then(data=>{
            console.log(data);
            this.adminList=data.data;
          
        })

    }

    addUser=(user)=>{
        let result = fetch('/user/add',{
            method:'post',
            headers: {
                'Content-Type': 'application/json'
                },
            body: JSON.stringify(user)
        })
        .then(res => {
            return res.json();
         })
         .then(data=>{
             console.log(data);
         })
         return result;

    }

    addReader=(reader)=>{
        let result=fetch('/reader/add',{
            method:'post',
            headers: {
                'Content-Type': 'application/json'
                },
            body: JSON.stringify(reader)
        })
        .then(res => {
           return res.json();
        })
        .then(data=>{
            console.log(data);
        })
        return result;
    }

    addLibrarian=(librarian)=>{
        let result=fetch('/librarian/add',{
            method:'post',
            headers: {
                'Content-Type': 'application/json'
                },
            body: JSON.stringify(librarian)
        })
        .then(res => {
           return res.json();
        })
        .then(data=>{
            console.log(data);
        })
        return result;
    }

    deleteReader=(reader_id)=>{
        let result=fetch('/reader/delete',{
            method:'post',
            headers: {
                'Content-Type': 'application/json'
                },
            body: JSON.stringify({reader_id:reader_id})
        })
        .then(res => {
           return res.json();
        })
        .then(data=>{
            console.log(data);
        })
        this.readerList=this.readerList.filter((record)=>{return record.reader_id!==reader_id;});
        return result;
    }

    deleteLibrarian=(librarian_id)=>{
        let result=fetch('/librarian/delete',{
            method:'post',
            headers: {
                'Content-Type': 'application/json'
                },
            body: JSON.stringify({employee_id:librarian_id})
        })
        .then(res => {
           return res.json();
        })
        .then(data=>{
            console.log(data);
        })
        this.adminList=this.adminList.filter((record)=>{return record.employee_id!==librarian_id;});
        return result;

    }
    
    // user只能改user个人信息，libraian可以改reader的详细信息
     modifyUser=(user)=>{
        let result=fetch("/user/modify",{   
            method:'post',
            headers: {
            'Content-Type': 'application/json',
          },
            body:JSON.stringify(user)
        }).then(res=>res.json()).then(data=>{
            console.log(data)
            return data.data;
        })
        this.readerList=this.readerList.map((record)=>{
            if(record.user_id!==user.user_id) return record;
            else return user;
        });
        return result;

     }


     modifyReader=(user)=>{
        let result=fetch("/reader/modify",{   
            method:'post',
            headers: {
            'Content-Type': 'application/json',
          },
            body:JSON.stringify(user)
        }).then(res=>res.json()).then(data=>{
            console.log(data)
            return data.data;
        })
        this.readerList=this.readerList.map((record)=>{
            if(record.reader_id!==user.reader_id) return record;
            else return user;
        });
        return result;

     }

     modifyLibrarian=(librarian)=>{
        let result=fetch("/librarian/modify",{   
            method:'post',
            headers: {
            'Content-Type': 'application/json',
          },
            body:JSON.stringify(librarian)
        }).then(res=>res.json()).then(data=>{
            console.log(data)
            return data.data;
        })
        this.adminList=this.adminList.map((record)=>{
            if(record.employee_id!==librarian.employee_id) return record;
            else return librarian;
        });
        return result;

     }




}
let userStore=new UserStore();
export default userStore