import { observable, action,makeAutoObservable ,toJS, computed, get} from 'mobx';

class BookStore{
    // mobx的map对象在react使用需要转换toJs
     bookList=[];
     bookCategory=[];
     personalBorrowList=[];
     borrowList=[];    
     count=0;

     constructor(){
        makeAutoObservable(this);
     }

     get filterCategory(){
        return this.bookCategory.map((record)=>{
            let item={};
            item.value=record.book_count;
            item.name=record.category_name;
            return item;
      });

     }

    //  增删改
    addBook=(book)=>{
        book.status=1
        book.category_id=parseInt(book.category_id);
        
        let result=fetch('/book/add',{
            method:'post',
            headers: {
                'Content-Type': 'application/json',
              },
            body: JSON.stringify(book)
        }).then((res)=>{return res.json();
        }).then((data)=>{
            console.log(data);
            return data.data;

        }).catch((error)=>{
            console.log(error)
            return null;
        })
        return result;

     }
     deleteBook=(book_id)=>{
        let result=fetch('/book/delete',{
            method:'post',
            headers: {
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({book_id:book_id})
        }).then((res)=>{return res.json();
        }).then((data)=>{
            console.log(data);
            return data.data;

        }).catch((error)=>{
            console.log(error)
            return null;
        })
        // 删除图书
        this.bookList=this.bookList.filter((record)=>{return record.book_id!==book_id;});
        return result;

     }
     modifyBook=(newRecord)=>{
        let result=fetch('/book/modify',{
            method:'post',
            headers: {
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(newRecord)
        }).then((res)=>{return res.json();
        }).then((data)=>{
            console.log(data);
            return data.data;
        }).catch((error)=>{
            console.log(error)
            return null;
        })
        this.bookList=this.bookList.map((record)=>{
            if(record.book_id!==newRecord.book_id) return record;
            else return newRecord;
        });
        return result;

     }

     modifyBorrow=(newRecord)=>{
        let result=fetch('/circulate/borrow/modify',{
            method:'post',
            headers: {
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(newRecord)
        }).then((res)=>{return res.json();
        }).then((data)=>{
            console.log(data);
            return data.data;
        }).catch((error)=>{
            console.log(error)
            return null;
        })
        this.borrowList=this.borrowList.map((record)=>{
            if(record.borrow_id!==newRecord.borrow_id) return record;
            else return newRecord;
        });
        return result;

     }



     borrowbook=async(borrow)=>{
      console.log(borrow);
        let result=fetch('/circulate/borrow',{
            method:'post',
            headers: {
                'Content-Type': 'application/json',
              },
            body:JSON.stringify(borrow)
        }).then((res)=>{return res.json();
        }).then((data)=>{
            console.log(data);
            return data.message;

        }).catch((error)=>{
            console.log(error);
            return null;
        })
        return result;

     }

     returnBook=(borrow)=>{
        this.borrowList=this.borrowList.map((record)=>{
            if(record.borrow_id!==borrow.borrow_id) return record;
            else return borrow;
        })
        console.log(borrow);
        let result=fetch('/circulate/return',{
            method:'post',
            headers: {
                'Content-Type': 'application/json',
              },
            body:JSON.stringify(borrow)
        }).then((res)=>{return res.json();
        }).then((data)=>{
            return data.data;
        }).catch((error)=>{
            console.log(error);
            return null;
        })
       
        return result;

     }

    //  书籍分类
    classfyBook=()=>{
        fetch('/book/category')
        .then(res => {
           return res.json();
        })
        .then(data=>{
           this.bookCategory=data.data;
           console.log(data.data)
           return data.message;
        })
        
     }

     getBorrowList=()=>{
        fetch('/circulate/borrow/list')
        .then(res => {
           return res.json();
        })
        .then(data=>{
           this.borrowList=data.data;
  
           return data.message;
        })
     }


     getBorrowById=(reader_id)=>{
        fetch('/circulate/borrow/list/'+reader_id)
        .then(res => {
           return res.json();
        })
        .then(data=>{
           this.borrowList=data.data;
           return data.message;
        })

     }




     getBookList=()=>{
        fetch('/book/list')
            .then(res => {
               return res.json();
            })
            .then(data=>{
                this.bookList=data.data;
                return data.message;
            })
     }



    // @action getBookList(){
    //     fetch('/book/list')
    //     .then(res => {
    //         if(res.status==200){
    //             return res.json();
    //         }
    //     })
    //     .then(data=>{
    //         console.log(data);
    //         this.bookList=data;
    //     })
    // }


}
let bookStore=new BookStore();
export default bookStore