import { SearchOutlined } from '@ant-design/icons';
import { Button, Input, Space, Table } from 'antd';
import { useRef, useState } from 'react';
import Highlighter from 'react-highlight-words';

export const book_data = [
    {
      key: '00001',
      book_id: "00001",
      book_name: '活着',
      author:"余华",
      category:"文学",
      status:"未借出"

    },
    {
      key: '00002',
      book_id: "00002",
      book_name: '上下五千年',
      author:"林汉达",
      category:"历史",
      status:"已借出"
    },
    {
      key: '00003',
      book_id: "00003",
      book_name: '马克思主义基本原理',
      author:"马克思",
      category:"哲学",
      status:"未借出"
    },
    {
      key: '00004',
      book_id: "00004",
      book_name: '马克思主义基本原理',
      author:"马克思",
      category:"哲学",
      status:"未借出"
    },
    {
      key: '00005',
      book_id: "00005",
      book_name: '马克思主义基本原理',
      author:"马克思",
      category:"哲学",
      status:"未借出"
    },
    {
      key: '00006',
      book_id: "00006",
      book_name: '马克思主义基本原理',
      author:"马克思",
      category:"哲学",
      status:"未借出"
    },
    {
      key: '00007',
      book_id: "00007",
      book_name: '马克思主义基本原理',
      author:"马克思",
      category:"哲学",
      status:"未借出"
    },
    {
      key: '00008',
      book_id: "00008",
      book_name: '马克思主义基本原理',
      author:"马克思",
      category:"哲学",
      status:"未借出"
    },
    {
      key: '00009',
      book_id: "00009",
      book_name: '马克思主义基本原理',
      author:"马克思",
      category:"哲学",
      status:"未借出"
    },
    {
      key: '000010',
      book_id: "000010",
      book_name: '马克思主义基本原理',
      author:"马克思",
      category:"哲学",
      status:"未借出"
    },
    {
      key: '00011',
      book_id: "00011",
      book_name: '马克思主义基本原理',
      author:"马克思",
      category:"哲学",
      status:"未借出"
    },
    {
      key: '00012',
      book_id: "00012",
      book_name: '马克思主义基本原理',
      author:"马克思",
      category:"哲学",
      status:"未借出"
    },
  ];
  export const  admin_data=[
    {
      key: '200201',
      id: "200201",
      name: '李华',
      phone:"18056177809"
    },
  ]

  export const reader_data=[
  {
    key: '2020212205156',
    id: "2020212205156",
    name: '张佳豪',
    age:21,
    sex:"男",
    phone:"19845674321",
    dept:"CS"

  },
  {
    key: '2020212205147',
    id: "2020212205147",
    name: '刘浏',
    age:66,
    sex:"男",
    phone:"19845674321",
    dept:"CS"
  },
  {
    key: '2020212205151',
    id: "2020212205151",
    name: '邹成明',
    age:20,
    sex:"男",
    phone:"19845674321",
    dept:"CS"
  },
  ];


  export const borrow_data = [
    {
      key: '0000000001',
      id: "0000000001",
      book_id:'00001',
      reader_id:"20020820",
      reader_type: 2,
      borrow_date:"2020-12-15",
      return_date:"2020-12-16",
      status:"已归还",
     
    },
    {
      key: '0000000002',
      id: "0000000002",
      book_id:'00002',
      reader_id:"2020212205151",
      reader_type: 3,
      borrow_date:"2020-12-15",
      return_date:"",
      status:"未归还",
      
    },
  ].map((record)=>{
    // 获取天数差
    var endTime=new Date().getTime();
    var starTime=new Date(record.borrow_date).getTime();
    var diff=parseInt((endTime-starTime)/(1000 * 60 * 60 * 24));
    console.log("diff"+diff)
    if(record.reader_type===2){
      if(diff<=60){
        record.overdue=0;
      }
      else{
        record.overdue=1;
        record.diff=diff;
        record.fine=(0.1*(diff-60)).toFixed(1);
      }
    }
    else if(record.reader_type===3){
      if(diff<=30){
        record.overdue=0;
      }
      else{
        record.overdue=1;
        record.diff=diff;
        record.fine=(0.1*(diff-30)).toFixed(1);
        
      }
    }
    return record

    //   record.overdue=1;
    // }

  });
  // 对borrow_date进行map操作，计算是否逾期以及罚款
  
